import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';
// import Swal from 'sweetalert2';

export default function Register() {

    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);

    // to store values of the input fields
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const [isAdmin, setIsAdmin] = useState(false);
    const [mobileNo, setMobileNo] = useState("");
   
// const register = (courseId) => {
//         fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
//             method: "POST",
//             headers: {
//                 'Content-Type': 'application/json',
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
               

//                 firstName: firstName,
//                 lastName: lastName,
//                 email: email,
//                 mobileNo:mobileNo,
//                 password: password
//             })
//         })
//         .then(res => res.json())
//         .then(data => {
//             console.log(data)

//             if(data === true) {
//                 Swal.fire({
//                     title: "Successfully enrolled",
//                     icon: "success",
//                     text: "You have successfully enrolled for this course."
//                 })

//                 navigate("/courses")

//             } else {
//                 Swal.fire({
//                     title: "Something went wrong",
//                     icon: "error",
//                     text: "Please try again."
//                 })
//             }

//         })
//     };


    useEffect(() => {
        if((email !== "" && password1 !== "" && firstName!=="" && lastName !== "" && mobileNo !== "" && password2) && (password1 === password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);

        }
    }, [firstName, lastName, email, password1, password2, mobileNo]);

   
    // function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading       
        e.preventDefault();

        // Clear input fields
        setFirstName("");
        setLastName("");
        setEmail("");
        setPassword1("");
        setPassword2("");
        setMobileNo("");
        

        // alert('Thank you for registering!');


        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo:mobileNo,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true) {
                // Swal.fire({
                //     title: "Successfully registered",
                //     icon: "success",
                //     text: "You have been Successfully registered."
                // })

                navigate("/courses")

            } else {
                // Swal.fire({
                //     title: "Something went wrong",
                //     icon: "error",
                //     text: "Please try again."
                // })
            }

        })
    };
  


    return (
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            
             <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="Enter firstname" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
                {/*<Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>*/}
            </Form.Group>

             <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="Enter lastname" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
                {/*<Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>*/}
            </Form.Group>


            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>password2</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile No</Form.Label>
                <Form.Control 
                    type="Text" 
                    placeholder="Mobile No" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>


            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

};


