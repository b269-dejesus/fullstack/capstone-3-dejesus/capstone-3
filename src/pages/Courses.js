// import coursesData from '../data/coursesData';//
import	{useState, useEffect} from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	const [courses, setCourses] = useState([]); 

	//console.log(coursesData);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course = {course}/>
	// 		)
	// })


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])



	return (
		<>	
		
			{courses}

		</>

		)

};

